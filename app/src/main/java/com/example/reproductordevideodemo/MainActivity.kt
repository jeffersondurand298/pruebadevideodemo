package com.example.reproductordevideodemo

import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.MediaController
import android.widget.VideoView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val mediaController = MediaController(this)
        mediaController.setAnchorView(videoView)

        val onlineUri = Uri.parse("https://www.youtube.com/watch?v=QWk6HfHl3rM")
        val offlineUri = Uri.parse("android.resource://$packageName/${R.raw.tutorialvideot}")

        videoView.setMediaController(mediaController)
        videoView.setVideoURi(offlineUri)
        videoView.requestFocus()
        videoView.start()



    }
}